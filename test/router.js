const Router = require('../lib/router');

describe('Router', () => {
  describe('id computation', () => {
    const getRouter = (code, file, _ast) => {
      const script = parse(code, { range: true, loc: true });
      const ast = Object.assign(Object.assign({}, script.body[0].expression), _ast);
      return new Router({ file, ast });
    };

    const getId = (...args) => getRouter(...args).id;

    it('has an id', () => {
      const id = getId(`obj.isFeatureEnabled('toggle-name')`, 'foo.js');
      expect(id).to.match(/^toggle-name-[a-z0-9]{64}$/);
    });

    it('computes a different id when in two different files', () => {
      const idOne = getId(`obj.isFeatureEnabled(constant.featureName)`, 'foo.js');
      const idTwo = getId(`obj.isFeatureEnabled(constant.featureName)`, 'bar.js');
      expect(idOne).to.not.eq(idTwo);
    });

    it('computes a different id when toggle names are different', () => {
      const idOne = getId(`obj.isFeatureEnabled('toggle-name')`, 'foo.js');
      const idTwo = getId(`obj.isFeatureEnabled('another-toggle')`, 'foo.js');
      expect(idOne).to.not.eq(idTwo);
    });

    it('computes the same id when toggle names are the same with same AST', () => {
      const idOne = getId(`obj.isFeatureEnabled('toggle-name')`, 'foo.js', { line: 1 });
      const idTwo = getId(`obj.isFeatureEnabled('toggle-name')`, 'foo.js', { line: 1 });
      expect(idOne).to.eq(idTwo);
    });

    it('computes a different id when toggle names are the same with different AST', () => {
      const idOne = getId(`obj.isFeatureEnabled('toggle-name')`, 'foo.js', { line: 1 });
      const idTwo = getId(`obj.isFeatureEnabled('toggle-name')`, 'foo.js', { line: 20 });
      expect(idOne).to.not.eq(idTwo);
    });

    it('computes the same common_id values when toggle is in two different files', () => {
      const commonIdOne = getRouter(`obj.isFeatureEnabled('toggle-name')`, 'foo.js').common_id;
      const commonIdTwo = getRouter(`obj.isFeatureEnabled('toggle-name')`, 'bar.js').common_id;
      expect(commonIdOne).to.exist;
      expect(commonIdOne).to.eq(commonIdTwo);
    });

    it('computes the same common_id values when toggle is in the same file', () => {
      const commonIdOne = getRouter(`obj.isFeatureEnabled('toggle-name')`, 'foo.js').common_id;
      const commonIdTwo = getRouter(`obj.isFeatureEnabled('toggle-name')`, 'foo.js').common_id;
      expect(commonIdOne).to.exist;
      expect(commonIdOne).to.eq(commonIdTwo);
    });
  });

  describe('.isRouter', () => {
    it('returns true when a Router', () => {
      const script = parse(`obj.isFeatureEnabled('toggle-name')`);
      expect(Router.isRouter(script.body[0].expression)).to.be.true;
    });
  });
});
