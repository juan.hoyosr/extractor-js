const Point = require('../lib/point');

describe('Point', () => {
  describe('id computation', () => {
    const getPoint = (code, file, _ast) => {
      const script = parse(code, { range: true, loc: true });
      const ast = Object.assign(Object.assign({}, script.body[0]), _ast);
      return new Point({ file, ast });
    };

    const getId = (...args) => getPoint(...args).id;

    it('has an id', () => {
      const id = getId(`
        if (a && obj.isFeatureEnabled('toggle-name') || b) {
          // do-something
        }
      `, 'foo.js');

      expect(id).to.exist;
    });

    it('computes a different id when in two different files', () => {
      const idOne = getId(`
        if (a && obj.isFeatureEnabled('toggle-name') || b) {
          // do-something
        }
      `, 'foo.js');

      const idTwo = getId(`
        if (a && obj.isFeatureEnabled('toggle-name') || b) {
          // do-something
        }
      `, 'bar.js');

      expect(idOne).to.not.eq(idTwo);
    });

    it('computes a different id when logical expressions are different', () => {
      const idOne = getId(`
        if (a && obj.isFeatureEnabled('toggle-name') || b) {
          // do-something
        }
      `, 'foo.js');

      const idTwo = getId(`
        if (a && obj.isFeatureEnabled('another-toggle') || b) {
          // do-something
        }
      `, 'foo.js');

      expect(idOne).to.not.eq(idTwo);
    });

    it('computes the same id when points are the same with same AST', () => {
      const idOne = getId(`
        if (a && obj.isFeatureEnabled('toggle-name') || b) {
          // do-something
        }
      `, 'foo.js', { line: 1 });

      const idTwo = getId(`
        if (a && obj.isFeatureEnabled('toggle-name') || b) {
          // do-something
        }
      `, 'foo.js', { line: 1 });

      expect(idOne).to.eq(idTwo);
    });

    it('computes a different id when points are the same with different AST', () => {
      const idOne = getId(`
        if (a && obj.isFeatureEnabled('toggle-name') || b) {
          // do-something
        }
      `, 'foo.js', { line: 1 });

      const idTwo = getId(`
        if (a && obj.isFeatureEnabled('toggle-name') || b) {
          // do-something
        }
      `, 'foo.js', { line: 20 });

      expect(idOne).to.not.eq(idTwo);
    });

    it('computes the same common_id values when similar points are in two different files', () => {
      const commonIdOne = getPoint(`
        if (a && obj.isFeatureEnabled('toggle-name') || b) {
          // do-something
        }
      `, 'foo.js').common_id;

      const commonIdTwo = getPoint(`
        if (a && obj.isFeatureEnabled('toggle-name') || b) {
          // do-something
        }
      `, 'bar.js').common_id;

      expect(commonIdOne).to.exist;
      expect(commonIdOne).to.eq(commonIdTwo);
    });

    it('computes the same common_id values when toggle is in the same file', () => {
      const commonIdOne = getPoint(`
        if (a && obj.isFeatureEnabled('toggle-name') || b) {
          // do-something
        }
      `, 'foo.js').common_id;

      const commonIdTwo = getPoint(`
        if (a && obj.isFeatureEnabled('toggle-name') || b) {
          // do-something
        }
      `, 'foo.js').common_id;

      expect(commonIdOne).to.exist;
      expect(commonIdOne).to.eq(commonIdTwo);
    });
  });

  describe('.isPoint', () => {
    it('returns true when a Point exists in the test expression', () => {
      const script = parse(`
      if (a && obj.isFeatureEnabled('toggle-name') || b) {
        // do-something
      }
      `);
      expect(Point.isPoint(script.body[0])).to.be.true;
    });

    it('returns true when the Router is negated', () => {
      const script = parse(`
      if (!obj.isFeatureEnabled('toggle-name') || b) {
        // do-something
      }
      `);
      expect(Point.isPoint(script.body[0])).to.be.true;
    });

    it('returns false when no Point exists in the test expression', () => {
      const script = parse(`
      if (a('foo') || b) {
        // do-something
      }
      `);
      expect(Point.isPoint(script.body[0])).to.be.false;
    });
  });
});
