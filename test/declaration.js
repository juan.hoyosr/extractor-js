const Declaration = require('../lib/declaration');

describe('Declaration', () => {
  describe('id computation', () => {
    const getDeclaration = (code, file, _ast) => {
      const script = parse(code, { range: true, loc: true });
      const ast = Object.assign(
        Object.assign({}, script.body[0].declarations[0].init.properties[0]), _ast
      );
      return new Declaration({ file, ast });
    };

    const getId = (...args) => getDeclaration(...args).id;

    it('has an id', () => {
      const id = getId(`
        const toggles = {
          toggle: envVarIncludes(pe, 'FOO'),
        };
      `, 'foo.js');

      expect(id).to.exist;
      expect(id).to.match(/^toggle-[a-z0-9]{64}$/);
    });

    it('computes a different id when in two different files', () => {
      const idOne = getId(`
        const toggles = {
          toggle: envVarIncludes(pe, 'FOO'),
        };
      `, 'foo.js');

      const idTwo = getId(`
        const toggles = {
          toggle: envVarIncludes(pe, 'FOO'),
        };
      `, 'bar.js');

      expect(idOne).to.not.eq(idTwo);
    });

    it('computes a different id when toggle names are different', () => {
      const idOne = getId(`
        const typeA = {
          toggle_one: envVarIncludes(pe, 'FOO'),
        };
      `, 'foo.js');

      const idTwo = getId(`
        const typeB = {
          toggle_two: envVarIncludes(pe, 'FOO'),
        };
      `, 'foo.js');

      expect(idOne).to.not.eq(idTwo);
    });

    it('computes the same id when toggle names are the same with same AST', () => {
      const idOne = getId(`
        const typeA = {
          toggle_one: envVarIncludes(pe, 'FOO'),
        };
      `, 'foo.js', { line: 1 });

      const idTwo = getId(`
        const typeB = {
          toggle_one: envVarIncludes(pe, 'FOO'),
        };
      `, 'foo.js', { line: 1 });

      expect(idOne).to.eq(idTwo);
    });

    it('computes a different id when toggle names are the same with different AST', () => {
      const idOne = getId(`
        const typeA = {
          toggle_one: envVarIncludes(pe, 'FOO'),
        };
      `, 'foo.js', { line: 1 });

      const idTwo = getId(`
        const typeB = {
          toggle_two: envVarIncludes(pe, 'FOO'),
        };
      `, 'foo.js', { line: 20 });

      expect(idOne).to.not.eq(idTwo);
    });

    it('computes the same common_id values when similar declarations are in two different files', () => {
      const commonIdOne = getDeclaration(`
        const toggles = {
          toggle: envVarIncludes(pe, 'FOO'),
        };
      `, 'foo.js').common_id;

      const commonIdTwo = getDeclaration(`
        const toggles = {
          toggle: envVarIncludes(pe, 'FOO'),
        };
      `, 'bar.js').common_id;

      expect(commonIdOne).to.exist;
      expect(commonIdOne).to.eq(commonIdTwo);
    });

    it('computes the same common_id values when similar declarations are in the same file', () => {
      const commonIdOne = getDeclaration(`
        const typeA = {
          toggle: envVarIncludes(pe, 'FOO'),
        };
      `, 'foo.js').common_id;

      const commonIdTwo = getDeclaration(`
        const typeB = {
          toggle: envVarIncludes(pe, 'FOO'),
        };
      `, 'foo.js').common_id;

      expect(commonIdOne).to.exist;
      expect(commonIdOne).to.eq(commonIdTwo);
    });
  });

  describe('.isDeclaration', () => {
    it('returns true when an envVarIncludes Declaration exists in the test expression', () => {
      const script = parse(`
      const toggles = {
        toggle: envVarIncludes(pe, 'FOO'),
      };
      `);
      expect(Declaration.isDeclaration(script.body[0].declarations[0].init.properties[0])).to.be.true;
    });

    it('returns true when an environmentVariableTrue Declaration exists in the test expression', () => {
      const script = parse(`
      const toggles = {
        toggle: environmentVariableTrue(pe, 'FOO'),
      };
      `);
      expect(Declaration.isDeclaration(script.body[0].declarations[0].init.properties[0])).to.be.true;
    });

    it('returns false when no Declaration exists in the test expression', () => {
      const script = parse(`
      const baz = {
        property: setValue(pe, 'FOO'),
      };
      `);
      expect(Declaration.isDeclaration(script.body[0])).to.be.false;
    });
  });
});
