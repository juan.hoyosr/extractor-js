const ToggleArea = require('../lib/toggle-area');

describe('ToggleArea', () => {
  const getHash = (code) => {
    const script = parse(code, { range: true, loc: true });
    const ast = script.body[0].declarations[0].init.properties[0];
    return new ToggleArea({ file: 'foo.js', ast }).hash;
  };

  it('computes a hash from the ast', () => {
    const hash = getHash(`
      const toggles = {
        toggle: envVarIncludes(pe, 'FOO'),
      };
    `);
    expect(hash).to.exist;
  });

  it('keeps hash the same if definitions do not change', () => {
    const hashOne = getHash(`
      const toggles = {
        toggle: envVarIncludes(pe, 'FOO'),
      };
    `);
    const hashTwo = getHash(`
      const toggles = {
        toggle: envVarIncludes(pe, 'FOO'),
      };
    `);
    expect(hashOne).to.eq(hashTwo);
  });

  it('computes a new hash if definition has changed', () => {
    const hashOne = getHash(`
      const toggles = {
        toggle: envVarIncludes(pe, 'FOO'),
      };
    `);
    const hashTwo = getHash(`
      const toggles = {
        toggle: envVarIncludes(pe, 'BAR'),
      };
    `);
    expect(hashOne).to.not.eq(hashTwo);
  });
});
