const ToggleArea = require('./toggle-area');
const crypto = require('crypto');

class Router extends ToggleArea {
  constructor(options) {
    super(options);

    this.type = this.constructor.name;
  }

  _id() {
    const hash = crypto.createHash('sha256');
    hash.update(this.file);
    hash.update(JSON.stringify(this.ast));
    return `${this.ast.arguments[0].value}-${hash.digest('hex')}`;
  }

  _commonId() {
    const ast = Object.assign({}, this.ast);
    const cleanAST = JSON.stringify(this._removeLocationKeys(ast));
    const hash = crypto.createHash('sha256');
    hash.update(cleanAST);
    return `${this.ast.arguments[0].value}-${hash.digest('hex')}`;
  }

  static isRouter(node) {
    // object.isFeatureEnabled(...)
    return (node.type === 'CallExpression'
    && node.callee.type === 'MemberExpression'
    && node.callee.property.type === 'Identifier'
    && node.callee.property.name === 'isFeatureEnabled');
  }
}

module.exports = Router;
