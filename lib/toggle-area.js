const crypto = require('crypto');
const cleanupAST = /,?"range":\[.+?\],?|,?"loc":\{"start":\{.+?\},"end":\{.+?\}\},?/g;

class ToggleArea {
  constructor({ file, start, end, ast }) {
    this.file = file;
    this.start = start;
    this.end = end;
    this.ast = ast;
    this.id = this._id(); // unique across all usages
    this.common_id = this._commonId(); // repeated in all instances across all usages
    /*
    * The hash is useful to compare toggles with a higher degree of granularity.
    * It is used instead of the common_id, despite its similarity (ast + no location
    * fingerprints), because the common_id can strip valuable parts of the ast that
    * could be needed for detailed comparisson purposes across toggles.
    */
    this.hash = this._computeHash(ast);
    this.original_id = null; // references the first toggle version, for diff purposes
  }

  _id() {
    if (this.constructor.name !== 'ToggleArea') throw new Error('A specific _id implementation is required');
  }

  _commonId() {
    if (this.constructor.name !== 'ToggleArea') throw new Error('A specific _commonId implementation is required');
  }

  _computeHash(ast) {
    const cleanedAST = this._removeLocationKeys(ast);
    const hash = crypto.createHash('sha256');
    hash.update(cleanedAST);
    return hash.digest('hex');
  }

  _removeLocationKeys(ast) {
    return JSON.stringify(ast).replace(cleanupAST, '');
  }
}

module.exports = ToggleArea;
