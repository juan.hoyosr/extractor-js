const ToggleArea = require('./toggle-area');
const Router = require('./router');
const crypto = require('crypto');

const logicalExpressionHasRouter = (node) => {
  if (node.type === 'LogicalExpression') {
    return (
      logicalExpressionHasRouter(node.left) ||
      logicalExpressionHasRouter(node.right)
    );
  } else if (node.type === 'UnaryExpression') {
    return logicalExpressionHasRouter(node.argument);
  } else {
    return Router.isRouter(node);
  }
};

class Point extends ToggleArea {
  constructor(options) {
    super(options);

    this.type = this.constructor.name;
  }

  _id() {
    const hash = crypto.createHash('sha256');
    hash.update(this.file);
    hash.update(JSON.stringify(this.ast));
    return hash.digest('hex');
  }

  _commonId() {
    const ifStatementAST = Object.assign({}, this.ast);
    delete ifStatementAST.consequent;
    delete ifStatementAST.alternate;
    const ifStatement = JSON.stringify(this._removeLocationKeys(ifStatementAST));
    const hash = crypto.createHash('sha256');
    hash.update(ifStatement);
    return hash.digest('hex');
  }

  static isPoint(node) {
    /*
    * if (... object.isFeatureEnabled(...) ...) {
    *
    * }
    */
   if (node.type !== 'IfStatement') return false;
    return logicalExpressionHasRouter(node.test);
  }
}

module.exports = Point;
