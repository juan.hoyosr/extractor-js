const ToggleArea = require('./toggle-area');
const crypto = require('crypto');

// TODO: decouple from refocus implementation and stop
// using these method matching
const declarationMethods = /envVarIncludes|environmentVariableTrue/;

class Declaration extends ToggleArea {
  constructor(options) {
    super(options);

    this.type = this.constructor.name;
  }

  _id() {
    const hash = crypto.createHash('sha256');
    hash.update(this.file);
    hash.update(JSON.stringify(this.ast));
    return `${this.ast.key.name}-${hash.digest('hex')}`;
  }

  _commonId() {
    return this.ast.key.name;
  }

  static isDeclaration(node) {
    /*
    * const toggles = {
    *   toggle: envVarIncludes(pe, 'FOO'),
    *   notAToggle: false,
    * };
    */
    return (node.type === 'Property'
    && node.value.type === 'CallExpression'
    && declarationMethods.test(node.value.callee.name));
  }
}

module.exports = Declaration;
