#!/usr/bin/env node

const program = require('commander');
const parse = require('./file-parser');
const IPCParser = require('./ipc-parser');

program
  .description('Extract the toggles of a JavaScript file in a specified directory')
  .arguments('<directory> <filepath>')
  .action((directory, filepath) => {
    parse(filepath, { cwd: directory }, (error, toggles) => {
      if (error) throw error;
      process.stdout.write(JSON.stringify(toggles));
    });
  });

program
  .command('ipc')
  .description('Extracts toggles through an IPC channel with other Node.JS process')
  .action(() => {
    IPCParser();
  });

program.parse(process.argv);
