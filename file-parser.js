const esprima = require('esprima');
const path = require('path');
const fs = require('fs');
const debug = require('debug')('parse-file');
const { Declaration, Point, Router } = require('./lib');

function extract(filename, toggles) {
  return (node, meta) => {
    const area = {
      file: filename,
      start: meta.start,
      end: meta.end,
      ast: node,
    };

    if (Declaration.isDeclaration(node)) {
      return toggles.push(new Declaration(area));
    }

    if (Router.isRouter(node)) {
      return toggles.push(new Router(area));
    }

    if (Point.isPoint(node)) {
      return toggles.push(new Point(area));
    }
  };
}

const parseOptions = { range: true, loc: true };

function parse(filename, { cwd = '' }, callback) {
  const toggles = [];
  debug('%s', filename)
  fs.readFile(path.join(cwd, filename), (err, data) => {
    if (err) return callback(err);
    try {
      esprima.parseModule(data.toString(), parseOptions, extract(filename, toggles));
    } catch (e) {
      // ignore parsing errors
    }
    callback(null, toggles);
  });
}

module.exports = parse;
