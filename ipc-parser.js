const fileParser = require('./file-parser');

function IPCParser() {
  if (!process.send) {
    throw new Error('No IPC channel found. Use spawn/fork.');
  }

  process.on('message', ({ action, payload }) => {
    switch (action) {
      case 'parse':
        const { filepath, directory } = payload;
        fileParser(filepath, { cwd: directory }, (error, toggles) => {
          if (error) throw error;
          process.send(toggles);
        });
        break;
      case 'end':
        process.exit(0);
        break;
    }
  });
}

module.exports = IPCParser;
